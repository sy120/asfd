Monitor consists of three parts: "amazon_ec2", "duke_colab", and "raspberry_pi". They are located in three folders.

to start read from the author's code, readers can start read from the cronjob

(1)
"amazon_ec2": 
(a)
cronjob is in /amazon_ec2/cronjob_ec2
(b)
"senddukemail" is to send emails with an without a link
"backup" contains the code to process emails with and without a link
"attach" contains the code to process emails with an attachment
"dnscheck" contains the code to process the DNS lookup time
"berry" contains the code to process the wifi time
(c)
all php files is in /amazon_ec2/web_dev/, in the author's project, they are located in /var/www/html/

(2)
duke_colab: 
(a)
cronjob is in /duke_colab/cronjob_colab
generate emails with an without a link from colab machine
(b)
"dnscheck" contains the code to generate dns lookup time and output txt files
"fine" contains the key to scp the files to Amazon EC2

(3)
cronjob is in /raspberry_pi/cronjob_rasp 
(with wired cable already plugged in)
configuration files are 
/raspberry_pi/network_interfaces, which originally in /etc/network/interfaces in Raspberry Pi
/raspberry_pi/wpa_supplicant, which originally in /etc/wpa_supplicant/wpa_supplicant.conf

