sudo chmod 755 /home/bitnami/dnscheck/dnsstart.sh
(time nslookup princeton.edu -timeout=10) &> /home/bitnami/dnscheck/inc_prin.txt
(time nslookup duke.edu -timeout=10) &> /home/bitnami/dnscheck/inc_duke.txt
(time nslookup pratt.duke.edu -timeout=10) &> /home/bitnami/dnscheck/inc_prat.txt
(time nslookup www.amazon.com -timeout=10) &> /home/bitnami/dnscheck/inc_amaz.txt
(time nslookup colab-sbx-376.oit.duke.edu -timeout=10) &> /home/bitnami/dnscheck/inc_colab.txt
(time nslookup ec2-52-23-207-225.compute-1.amazonaws.com -timeout=10) &> /home/bitnami/dnscheck/inc_ec2.txt
(time nslookup www.youku.com -timeout=10) &> /home/bitnami/dnscheck/inc_youku.txt
(time nslookup www.youtube.com -timeout=10) &> /home/bitnami/dnscheck/inc_youtube.txt
scp -i /home/bitnami/fine/fine/three/netava.pem /home/bitnami/dnscheck/* ubuntu@ec2-52-91-20-113.compute-1.amazonaws.com:/home/ubuntu/dnscheck/
