Internet Systems Consortium DHCP Client 4.3.1
Copyright 2004-2014 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/wlan0/90:8d:78:d8:7c:5b
Sending on   LPF/wlan0/90:8d:78:d8:7c:5b
Sending on   Socket/fallback
DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 7
DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 16
DHCPREQUEST on wlan0 to 255.255.255.255 port 67
DHCPOFFER from 1.1.1.5
DHCPACK from 1.1.1.5
Restarting ntp (via systemctl): ntp.service.
bound to 10.188.85.119 -- renewal in 783 seconds.

real	0m9.303s
user	0m0.080s
sys	0m0.060s
