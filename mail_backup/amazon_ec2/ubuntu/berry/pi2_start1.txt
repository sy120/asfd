Internet Systems Consortium DHCP Client 4.3.1
Copyright 2004-2014 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/wlan0/04:8d:38:2b:fd:86
Sending on   LPF/wlan0/04:8d:38:2b:fd:86
Sending on   Socket/fallback
DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 4
DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 6
DHCPREQUEST on wlan0 to 255.255.255.255 port 67
DHCPOFFER from 1.1.1.5
DHCPACK from 1.1.1.5
Restarting ntp (via systemctl): ntp.service.
bound to 10.188.82.43 -- renewal in 725 seconds.

real	0m7.568s
user	0m0.310s
sys	0m0.220s
