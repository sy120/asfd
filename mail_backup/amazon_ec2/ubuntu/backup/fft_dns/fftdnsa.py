#!/usr/bin/python

import MySQLdb
import numpy as np
test_time = []
princeton_edu = []
amazon_com = []
duke_pratt = []
youtube = []
youku = []
A_princeton_edu = []
A_amazon_com = []
A_duke_pratt = []
A_youtube = []
A_youku = []
i = 0

# Open database connection
db = MySQLdb.connect("dukedata.c9ezfdu0vj4c.us-east-1.rds.amazonaws.com","root","k2wrub6r","DUKE_MAIL" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

# Prepare SQL query to INSERT a record into the database.
sql = "SELECT * FROM dnscheck "
try:
   # Execute the SQL command
   cursor.execute(sql)
   # Fetch all the rows in a list of lists.
   results = cursor.fetchall()
   for row in results:
      test_time.append(row[0])
      princeton_edu.append(row[2])
      amazon_com.append(row[5])
      duke_pratt.append(row[6])
      youtube.append(row[7])
      youku.append(row[8])
      A_princeton_edu.append(row[10])
      A_amazon_com.append(row[13])
      A_duke_pratt.append(row[14])
      A_youtube.append(row[15])
      A_youku.append(row[16])
      i = i+1
      # Now print fetched result
      #print "test_time=%s,princeton_edu=%s,amazon_com=%s" % \
      #       (test_time, princeton_edu, amazon_com)
except:
   print "Error: unable to fecth data"

n = len(test_time);
fft_princeton_edu = np.fft.fft(princeton_edu)
fft_amazon_com = np.fft.fft(amazon_com)
fft_duke_pratt = np.fft.fft(duke_pratt)
fft_youtube = np.fft.fft(youtube)
fft_youku = np.fft.fft(youku)
fft_A_princeton_edu = np.fft.fft(A_princeton_edu)
fft_A_amazon_com = np.fft.fft(A_amazon_com)
fft_A_duke_pratt = np.fft.fft(A_duke_pratt)
fft_A_youtube = np.fft.fft(A_youtube)
fft_A_youku = np.fft.fft(A_youku)

cursor.execute("TRUNCATE TABLE fft_result")
for i in range(0,n-1):
#  cursor1 = db.cursor()
   # Prepare SQL query to INSERT a record into the database.
   # "INSERT INTO SES_BASELINE(DATE_ADD(Test_Time,INTERVAL 4 HOUR),\
   sql = "INSERT INTO fft_result(Test_Time,\
         fft_princeton_edu,\
         fft_amazon_com, \
         fft_duke_pratt,\
         fft_youtube, \
         fft_youku,\
         fft_A_princeton_edu,\
         fft_A_amazon_com, \
         fft_A_duke_pratt,\
         fft_A_youtube, \
         fft_A_youku)\
         VALUES ('%s', '%f', '%f', '%f', '%f', '%f', '%f', '%f', '%f', '%f', '%f')" % \
         (test_time[i], abs(fft_princeton_edu[i]), abs(fft_amazon_com[i]), abs(fft_duke_pratt[i]), abs(fft_youtube[i]),abs(fft_youku[i]),abs(fft_A_princeton_edu[i]),abs(fft_A_amazon_com[i]),abs(fft_A_duke_pratt[i]),abs(fft_A_youtube[i]),abs(fft_A_youku[i]))
         #('THL1', s2, 2, 3, 4)
   try:
         # Execute the SQL command
         cursor.execute(sql)
         # Commit your changes in the database
         db.commit()
   except:
         # Rollback in case there is any error
         db.rollback() 

# disconnect from server
db.close()



