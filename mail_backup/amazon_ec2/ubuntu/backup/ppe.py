import random
import sys
import os
import re
from array import *
import string
import csv
import MySQLdb

db = MySQLdb.connect("dukedata.c9ezfdu0vj4c.us-east-1.rds.amazonaws.com","root","k2wrub6r","DUKE_MAIL" )

# prepare a cursor object using cursor() method
cursor = db.cursor()
cursor.execute("DROP TABLE IF EXISTS chart1")
cursor.execute("DROP TABLE IF EXISTS chart2")
cursor.execute("DROP TABLE IF EXISTS chart3")
cursor.execute("DROP TABLE IF EXISTS charte1")
cursor.execute("DROP TABLE IF EXISTS charte2")
cursor.execute("DROP TABLE IF EXISTS charte3")

#Colab Machine 
#chart1
cursor.execute("create table chart1 as (select GOOGLE_LINK.Test_Time, GOOGLE_LINK.Google_Link_Delay, GOOGLE_LINK.L_Message_Id, BASELINE.Baseline_Delay, BASELINE.B_Message_Id FROM GOOGLE_LINK INNER JOIN BASELINE ON GOOGLE_LINK.Test_Time = BASELINE.Test_Time) order by Test_Time ASC")

#chart2
cursor.execute("create table chart2 as (select chart1.Test_Time, chart1.Google_Link_Delay, chart1.Baseline_Delay, chart1.L_Message_Id, chart1.B_Message_Id, SES_BASELINE.SES_Baseline_Delay, SES_BASELINE.W1_Message_Id FROM chart1 INNER JOIN SES_BASELINE ON chart1.Test_Time = SES_BASELINE.Test_Time) order by Test_Time ASC")

#chart3
cursor.execute("create table chart3 as (select chart2.Test_Time, chart2.Google_Link_Delay, chart2.Baseline_Delay, chart2.SES_Baseline_Delay, chart2.B_Message_Id, chart2.L_Message_Id, chart2.W1_Message_Id, SES_LINK.SES_Link_Delay, SES_LINK.W2_Message_Id FROM chart2 INNER JOIN SES_LINK ON chart2.Test_Time = SES_LINK.Test_Time) order by Test_Time ASC")

#charte1
cursor.execute("create table charte1 as (select GOOGLE_LINK.Test_Time, GOOGLE_LINK.Google_Link_End_to_End_Delay, GOOGLE_LINK.L_Message_Id, BASELINE.Baseline_End_to_End_Delay, BASELINE.B_Message_Id FROM GOOGLE_LINK INNER JOIN BASELINE ON GOOGLE_LINK.Test_Time = BASELINE.Test_Time) order by Test_Time ASC")

#charte2
cursor.execute("create table charte2 as (select charte1.Test_Time, charte1.Google_Link_End_to_End_Delay, charte1.Baseline_End_to_End_Delay, charte1.L_Message_Id, charte1.B_Message_Id, SES_BASELINE.SES_Baseline_End_to_End_Delay, SES_BASELINE.W1_Message_Id FROM charte1 INNER JOIN SES_BASELINE ON charte1.Test_Time = SES_BASELINE.Test_Time) order by Test_Time ASC")

#charte3
cursor.execute("create table charte3 as (select charte2.Test_Time, charte2.Google_Link_End_to_End_Delay, charte2.Baseline_End_to_End_Delay, charte2.SES_Baseline_End_to_End_Delay, charte2.B_Message_Id, charte2.L_Message_Id, charte2.W1_Message_Id, SES_LINK.SES_Link_End_to_End_Delay, SES_LINK.W2_Message_Id FROM charte2 INNER JOIN SES_LINK ON charte2.Test_Time = SES_LINK.Test_Time) order by Test_Time ASC")

db.close()

