today=`date '+%Y_%m_%d_%H_%M_%S'`;
s3_pdf64K="s3://songyang/pdf_64K/$today"
s3_pdf1M="s3://songyang/pdf_1M/$today"
s3_pptx64K="s3://songyang/pptx_64K/$today"
s3_pptx1M="s3://songyang/pptx_1M/$today"
s3_docx64K="s3://songyang/docx_64K/$today"
s3_docx1M="s3://songyang/docx_1M/$today"
aws s3 cp /data/pdf_64K1/ $s3_pdf64K --region us-east-1 --recursive
aws s3 cp /data/pdf_1M1/ $s3_pdf1M --region us-east-1 --recursive
aws s3 cp /data/ppt_64K1/ $s3_pptx64K --region us-east-1 --recursive
aws s3 cp /data/ppt_1M1/ $s3_pptx1M --region us-east-1 --recursive
aws s3 cp /data/docx_64K1/ $s3_docx64K --region us-east-1 --recursive
aws s3 cp /data/docx_1M1/ $s3_docx1M --region us-east-1 --recursive


