from pptx import Presentation
import time

prs = Presentation('/home/ubuntu/attach/ppt_group/1M.pptx')
title_slide_layout = prs.slide_layouts[0]
slide = prs.slides.add_slide(title_slide_layout)
title = slide.shapes.title
subtitle = slide.placeholders[1]

title.text = "Hello, World!"
subtitle.text = time.strftime("%c")

prs.save('/home/ubuntu/attach/ppt_group/1M1.pptx')

prs = Presentation('/home/ubuntu/attach/ppt_group/64K.pptx')
title_slide_layout = prs.slide_layouts[0]
slide = prs.slides.add_slide(title_slide_layout)
title = slide.shapes.title
subtitle = slide.placeholders[1]

title.text = "Hello, World!"
subtitle.text = time.strftime("%c")

prs.save('/home/ubuntu/attach/ppt_group/64K1.pptx')


